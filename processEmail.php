<?php include("email.php");?>
<?php
	$firstEmail = new email();

	$firstEmail->setRecipientAddress("pculp82@gmail.com");
	$firstEmail->setSenderAddress("system@patrickculpdev.com");
	$firstEmail->setEmailMessage("Hello Patrick");
	$firstEmail->setEmailSubject("First Email");
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<p><b>Sender:</b> <?php echo $firstEmail->getSenderAddress();?></p>
	<p><b>Recipient:</b> <?php echo $firstEmail->getRecipientAddress();?></p>
	<h2><b>Subject:</b> <?php echo $firstEmail->getEmailSubject();?></h2>
	<p><b>Message:</b> <?php echo $firstEmail->getEmailMessage();?></p>
	<p><?php $firstEmail->sendEmail($firstEmail->getRecipientAddress(),
					   $firstEmail->getEmailSubject(),
					   $firstEmail->getEmailMessage(),
					   $firstEmail->getSenderAddress()); echo "email sent.";?></p>
</body>
</html>