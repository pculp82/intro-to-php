<?php

class Emailer {
	//It will store info required to send a PHP email
	//It will build an email and use the PHP mail().

	//class properties:
	private $recipientAddress;

	private $emailSubject;

	private $emailMessage;

	private $senderAddress;

	//Constructer Funtion

	function __construct(){

	}

	//Setters AKA Mutators
	function setRecipientAddress($inMessage){
		$this->recipientAddress = $inMessage;
	}
	function setSenderAddress($inAdress){
		$this->senderAddress = $inAddress;
	}
	function setEmailMessage($inAddress){
		$this->emailMessage = $inAddress;
	}
	function setEmailSubject($inSubject){
		$this->emailSubject = $inSubject;
	}

	//Getters AKA Accessors
	function getRecipientAddress(){
		return $this->recipientAddress;
	}
	function getSenderAddress(){
		return $this->senderAddress;
	}
	function getEmailMessage(){
		return $this->emailMessage;		
	}
	function getEmailSubject(){
		return $this->emailSubject;
	}

	function sendEmail(){
		$fromAddress ="From: ".$this->getSenderAddress();
		mail($this->getRecipientAddress(),
			 $this->getEmailSubject(),
			 $this->getEmailMessage(),
			 $fromAddress
			 );
	}
}
?>