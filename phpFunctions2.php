<?php
	
	#1 Create a function that will accept a date input and format it into mm/dd/yyyy format.

	$timeStamp = time();
	function dateFormat($inDate){
	           
	    $day = date("d", $inDate);
	    $month = date("m", $inDate);
	    $year = date("Y",$inDate);
	    $dateStrng = $month."/".$day."/".$year;
	    return $dateStrng;
	}

	#2 Create a function that will accept a date input and format it into dd/mm/yyyy format to use when working with international dates.

	function euroDateFormat($inDate){
		$day = date("d", $inDate);
	    $month = date("m", $inDate);
	    $year = date("Y",$inDate);
	    $dateStrng = $day."/".$month."/".$year;
	    return $dateStrng;
	}
	#3 Create a function that will accept a string input.  It will do the following things to the string: -->
	        /*
	        X Display the number of characters in the string
	        X Trim any leading or trailing whitespace
	        X Display the string as all lowercase characters
	        - Will display whether or not the string contains "DMACC" either upper or lowercase */
	$stringForFormatting = "   fdashfdmaccjklDFSJFHKLshiofgwei";
	function stringPlay($inString){
		$stringLength = strlen($inString);
		$trimString = trim($inString);
		$string2LC = strtolower($inString);
		$MatchDMACC = strpos(strtoupper($inString), 'DMACC');
		$DMACCpresent = "found";
		if ($MatchDMACC == false) {
			$DMACCpresent = "missing";}

		return "  String Play- <br>String Length: ".$stringLength.", <br><br>Trim String: ".$trimString.", <br><br>String to Lowercase: ".$string2LC.", <br><br> Check string contents for 'dmacc': ".$DMACCpresent." <br><br>String in question:".$inString."<br>End String Play.";
	}

	#4 Create a function that will accept a number and display it as a formatted number.   Use 1234567890 for your testing. 
	$num2Format = 1234567890;
	function numberFormat($inNum){
		$formattedNumber = number_format("$inNum");
		return $formattedNumber;
	}


   
	#5 Create a function that will accept a number and display it as US currency.  Use 123456 for your testing.
	$num2Current = 123456;
	function currencyFormat($inNum){
		$formattedCurrency = "$".number_format("$inNum",2);
		return $formattedCurrency;
	} 
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
			<p><?php echo DateFormat($timeStamp);?></p>
			<p><?php echo euroDateFormat($timeStamp);?></p>
			<p><?php echo stringPlay($stringForFormatting);?></p>
			<p><?php echo numberFormat($num2Format);?></p>
			<p><?php echo currencyFormat($num2Current);?></p>
</body>
</html>