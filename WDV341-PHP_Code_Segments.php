<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 - PHP Code Segments</title>
</head>
<body>
	<h1>WDV341 - PHP Code Segments</h1>
	<h2>Example 1 - ALl output is written by PHP echo commands</h2>
	<?php
		$validInput = true;
		if(!$validInput){
			//true branch
			echo "<h1>Good Job! Thank you.</h1>";
		}else{
			//false branch
			echo "<h1>Sorry, Better luck next time.</h1>";
		}
	?>
	<h2>Example 2 - All output is passed to the Responce Object as HTML line</h2>

	<?php
		$validTwoInput = true; //boolean true;
		if("4"==4)
		{
	?>
	<h1>Good Job! Thank you.</h1>
	<p>It has been a pleasure serving you.  Thank you for your time.</p>
	<?php
		}
		else
		{
	?>
	<h1>Sorry, Better luck next time.</h1>
	<p>we appreciate your efforts and hope that you try again soon.  best regards.</p>
	<?php
		}
	?>
</body>
</html>